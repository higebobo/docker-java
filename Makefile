REPOSITORY=higebobo
IMAGE=oracle-java
TAG=6-jre-alpine
NAME=${IMAGE}-app
FILE=jre-6u45-linux-x64.bin

all: run

build:
	cp -p ../../files/jdk/${FILE} ./files
	docker build -t ${IMAGE}:${TAG} .
	rm -f ./files/${FILE}

run: build
	docker run -it --rm --name ${NAME} ${IMAGE}:${TAG}

down:
	docker rmi ${IMAGE}:${TAG}

test-java:
	docker run -it ${IMAGE}:${TAG} "java" "-version"

docker-login:
	docker login

docker-logout:
	docker logout

docker-push:
	docker tag ${IMAGE}:${TAG} ${REPOSITORY}/${IMAGE}:${TAG}
	docker push ${REPOSITORY}/${IMAGE}:${TAG}
	docker rmi ${REPOSITORY}/${IMAGE}:${TAG}

add:
	git add .

commit: add
	git commit -m 'modified'

push: commit push-only

push-first:
	git push -u origin master

push-only:
	git push origin master

push-with-tag:
	git push origin ${IMAGE}-${TAG}
