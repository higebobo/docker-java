# docker-java

Dockerfile for Java

## branch

* openjdk-14-alpine
    - Dockerhubのhigebobo/openjdk:14-alpineと連携
* oracle-java-6-jre-alpine
    - Dockerhubのhigebobo/oracle-javaにはイメージをプッシュ
    - filesにローカル環境からコピーしてbuildするのでDockerhubではbuildしない

## link

* [JDK 13 GA Release](https://jdk.java.net/13/)
* [JDK 14 Early\-Access Builds](https://jdk.java.net/14/)
